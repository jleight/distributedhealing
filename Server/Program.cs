﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Server.Services;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(MonitorService));
            host.Open();
            Console.WriteLine("Monitor service is listening...");
            Console.ReadKey(true);
            host.Close();
        }
    }
}
