﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class MonitorService : IMonitorService
    {
        private static Dictionary<string, List<IMonitorServiceCallback>> _channels;

        private IMonitorServiceCallback _client
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IMonitorServiceCallback>();
            }
        }


        public MonitorService()
        {
            if (_channels == null)
                _channels = new Dictionary<string, List<IMonitorServiceCallback>>();
        }


        public void Subscribe(string channel)
        {
            if (!_channels.ContainsKey(channel))
                _channels[channel] = new List<IMonitorServiceCallback>();

            _channels[channel].Add(_client);
            Console.WriteLine("Client subscribed to channel '" + channel + "[" + _channels[channel].Count + "]'");
        }

        public void Unsubscribe(string channel)
        {
            if (!_channels.ContainsKey(channel))
                return;

            _channels[channel].Remove(_client);
            Console.WriteLine("Client unsubscribed from channel '" + channel + "[" + _channels[channel].Count + "]'");
        }

        public void PublishVital(string channel, string vital, double value)
        {
            if (!_channels.ContainsKey(channel))
                return;

            foreach (var sub in _channels[channel])
            {
                try
                {
                    sub.ReceiveVital(channel, vital, value);
                }
                catch (Exception e)
                {
                    _channels[channel].Remove(sub);
                    Console.WriteLine(e);
                }
            }
        }

        public void PublishAlarm(string channel, string alarm)
        {
            if (!_channels.ContainsKey(channel))
                return;

            foreach (var sub in _channels[channel])
            {
                try
                {
                    sub.ReceiveAlarm(channel, alarm);
                }
                catch (Exception e)
                {
                    _channels[channel].Remove(sub);
                    Console.WriteLine(e);
                }
            }
        }

        public void PublishAcknowledgement(string channel, string acknowledgement)
        {
            if (!_channels.ContainsKey(channel))
                return;

            foreach (var sub in _channels[channel])
            {
                try
                {
                    sub.ReceiveAcknowledgement(channel, acknowledgement);
                }
                catch (Exception e)
                {
                    _channels[channel].Remove(sub);
                    Console.WriteLine(e);
                }
            }
        }
    }
}
