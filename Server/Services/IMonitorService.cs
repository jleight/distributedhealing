﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server.Services
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IMonitorServiceCallback))]
    public interface IMonitorService
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe(string channel);

        [OperationContract(IsOneWay = true)]
        void Unsubscribe(string channel);

        [OperationContract(IsOneWay = true)]
        void PublishVital(string channel, string vital, double value);

        [OperationContract(IsOneWay = true)]
        void PublishAlarm(string channel, string alarm);

        [OperationContract(IsOneWay = true)]
        void PublishAcknowledgement(string channel, string acknowledgement);
    }
}
