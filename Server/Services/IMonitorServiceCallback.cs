﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Server.Services
{
    interface IMonitorServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void ReceiveVital(string bedsideMonitor, string vital, double value);

        [OperationContract(IsOneWay = true)]
        void ReceiveAlarm(string bedsideMonitor, string alarm);

        [OperationContract(IsOneWay = true)]
        void ReceiveAcknowledgement(string bedsideMonitor, string acknowledgement);
    }
}
