﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace MonitorTUI
{
    public class UserInterface : DynamicObject
    {
        #region Native Functions
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeConsole();

        [DllImport("shell32.dll", SetLastError = true)]
        static extern IntPtr CommandLineToArgvW([MarshalAs(UnmanagedType.LPWStr)] string lpCmdLine, out int pNumArgs);
        #endregion

        #region Variables
        public bool IsBedsideMonitor { get; set; }
        public bool IsCentralMonitor
        {
            get
            {
                return !IsBedsideMonitor;
            }
            set
            {
                IsBedsideMonitor = !value;
            }
        }
        public bool IsDone { get; private set; }
        private IEnumerable<MethodInfo> _commands;
        private dynamic _monitor;

        public Action<string> Subscribe { get; set; }
        public Action<string> Unsubscribe { get; set; }
        public Action<string> Acknowledge { get; set; }
        public Action<string> Alarm { get; set; }
        public Action<string, double, double> SetLimits { get; set; }
        public Action Call { get; set; }
        #endregion


        public UserInterface(dynamic monitor)
        {
            _monitor = monitor;
            this.IsBedsideMonitor = this._monitor.IsPublisher;
            this.IsDone = false;

            _commands = typeof(UserInterface).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
                .Where((c) => c.Name.StartsWith("Command_"));

            _monitor.UpdateAlarm += (Action<string, string>)DisplayAlarm;
        }


        public void Go()
        {
            AllocConsole();
            MainLoop();
            FreeConsole();
        }

        public void DisplayAlarm(string bedsideMonitor, string alarm)
        {
            Console.WriteLine("New alarm for '" + bedsideMonitor + "': " + alarm + "!");
        }


        #region User Input
        public void MainLoop()
        {
            while (!this.IsDone)
            {
                // print out the prompt and read the input
                Console.Write("Monitor> ");
                var input = Console.ReadLine().Trim();

                // no comand was type, just continue
                if (input.Length == 0)
                    continue;

                // split the input into arguments
                int argc;
                var argv = CommandLineToArgvW(input, out argc);

                // if we get a null pointer, continue
                if (argv == IntPtr.Zero)
                    continue;

                // add the arguments to a list
                var args = new List<string>();
                try
                {
                    for (var i = 0; i < argc; i++)
                        args.Add(Marshal.PtrToStringUni(Marshal.ReadIntPtr(argv, i * IntPtr.Size)));
                }
                finally
                {
                    Marshal.FreeHGlobal(argv);
                }


                HandleInput(args.First(), args.Skip(1));
            }

        }

        public void HandleInput(string command, IEnumerable<string> args)
        {
            // find the method to call
            var method = _commands.FirstOrDefault((c) => c.Name.ToLower().Equals("command_" + command.ToLower()));

            if (method == null)
            {
                // command is invalid
                Console.WriteLine("Unknown command '" + command + "'");
            }
            else
            {
                // command is valid, invoke the method
                try
                {
                    method.Invoke(this, args.ToArray());
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid number or type of arguments specified.");
                }
            }

            // blnk line after every method
            Console.WriteLine();
        }
        #endregion

        #region Commands
        [UsageMessage("exits the monitor application")]
        private void Command_Exit()
        {
            this.IsDone = true;
        }

        [UsageMessage("displays this help message")]
        private void Command_Help()
        {
            Console.WriteLine("Welcome to the Monitor Interface!");
            Console.WriteLine();
            Console.WriteLine("The following commands can be used:");

            var commands = _commands.OrderBy(c => c.Name);
            foreach (var command in commands)
            {
                var name = command.Name.ToLower().Substring(8);
                var message = command.GetCustomAttributes(typeof(UsageMessageAttribute), false).FirstOrDefault() as UsageMessageAttribute;
                Console.WriteLine("\t" + name + " - " + message.Message);
            }
        }

        [UsageMessage("subscribe to a monitor")]
        private void Command_Subscribe(string channel)
        {
            if (channel == null)
            {
                Console.WriteLine("Usage: subscribe channel");
                return;
            }

            if (Subscribe == null)
            {
                Console.WriteLine("Critical Error: Subscribe delegate not provided");
                return;
            }

            Subscribe(channel);
            Console.WriteLine("Subscribed to channel '" + channel + "'");
        }

        [UsageMessage("unsubscribe from a monitor")]
        private void Command_Unsubscribe(string channel)
        {
            if (channel == null)
            {
                Console.WriteLine("Usage: unsubscribe channel");
                return;
            }

            if (Unsubscribe == null)
            {
                Console.WriteLine("Critical Error: Unsubscribe delegate not provided");
                return;
            }

            Unsubscribe(channel);
            Console.WriteLine("Unsubscribed from channel '" + channel + "'");
        }

        [UsageMessage("displays vital and alarm information")]
        private void Command_Display(string type, string format, string channel)
        {
            switch (type.ToLower())
            {
                case "vital":
                    DisplayVitals(format, channel);
                    break;
                case "alarm":
                    DisplayAlarms(format, channel);
                    break;
                default:
                    Console.WriteLine("Invalid display type. Valid types are: vital, alarm, ack");
                    break;
            }
        }

        [UsageMessage("displays whether or not this client is a publisher")]
        private void Command_Publisher()
        {
            Console.WriteLine(this.IsBedsideMonitor);
        }

        [UsageMessage("acknowledges an alarm")]
        private void Command_Ack(string alarm)
        {
            if (this.IsCentralMonitor)
            {
                Console.WriteLine("Alarms must be acknowledged from the bedside monitor.");
                return;
            }

            Acknowledge(alarm);
        }

        [UsageMessage("creates an alarm with the specified name")]
        private void Command_TriggerAlarm(string alarm)
        {
            if (this.IsCentralMonitor)
            {
                Console.WriteLine("Alarms can only be triggered from the bedside monitor.");
                return;
            }

            Alarm(alarm);
        }

        [UsageMessage("sets the lower and upper limits for a patient vital")]
        private void Command_Limit(string vital, string strLower, string strUpper)
        {
            if (this.IsCentralMonitor)
            {
                Console.WriteLine("Vital limits can only be set from the bedside monitor.");
                return;
            }

            double lower;
            double upper;
            if (!Double.TryParse(strLower, out lower) || !Double.TryParse(strUpper, out upper))
            {
                Console.WriteLine("Upper and lower limits must be numbers!");
                return;
            }

            SetLimits(vital, lower, upper);
        }

        [UsageMessage("acts as if the patient has pressed the call button")]
        private void Command_Call()
        {
            if (this.IsCentralMonitor)
                return;

            Call();
        }
        #endregion

        #region Command Helpers
        private void DisplayVitals(string format, string channel)
        {
            if (_monitor.Vitals == null || !_monitor.Vitals.ContainsKey(channel))
            {
                Console.WriteLine("There are no vitals to display.");
                return;
            }

            var vitals = _monitor.Vitals[channel];
            switch (format.ToLower())
            {
                case "last":
                    foreach (var vital in vitals)
                    {
                        Console.Write(vital.Key + " [");
                        var list = vital.Value.ToArray();
                        var value = list[list.Length - 1];
                        Console.WriteLine(value.Item1 + "]: " + value.Item2);
                    }
                    break;
                case "list":
                    foreach (var vital in vitals)
                    {
                        Console.WriteLine(vital.Key);
                        foreach (var value in vital.Value)
                        {
                            Console.WriteLine(value.Item1 + ": " + value.Item2);
                        }
                        Console.WriteLine();
                    }
                    break;
                default:
                    Console.WriteLine("Invalid format. Valid formats are: last, list");
                    break;
            }
        }

        private void DisplayAlarms(string format, string channel)
        {
            if (_monitor.Alarms == null || !_monitor.Alarms.ContainsKey(channel))
            {
                Console.WriteLine("There are no alarms to display.");
                return;
            }

            var alarms = _monitor.Alarms[channel].ToArray();
            if (alarms.Length == 0)
            {
                Console.WriteLine("There are no alarms to display.");
                return;
            }
            var alarm = alarms[alarms.Length - 1];

            switch (format.ToLower())
            {
                case "last":
                    Console.WriteLine(alarm.Item1 + ": " + alarm.Item2);
                    break;
                case "list":
                    foreach (var a in alarms)
                    {
                        Console.WriteLine(alarm.Item1 + ": " + a.Item2);
                    }
                    break;
                default:
                    Console.WriteLine("Invalid format. Valid formats are: last, list");
                    break;
            }
        }
        #endregion
    }

    [AttributeUsage(AttributeTargets.Method)]
    class UsageMessageAttribute : System.Attribute
    {
        public readonly string Message;

        public UsageMessageAttribute(string message)
        {
            this.Message = message;
        }
    }
}
