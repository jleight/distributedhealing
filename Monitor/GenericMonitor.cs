﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using System.Linq;
using Monitor.MonitorService;

namespace Monitor
{
    public class GenericMonitor : IMonitorServiceCallback
    {
        #region Variables
        private const int MaxVitalsToKeep = 25;

        public bool IsPublisher { get; private set; }
        public bool IsText { get; private set; }
        public Dictionary<string, Dictionary<string, List<Tuple<DateTime, double>>>> Vitals { get; private set; }
        public Dictionary<string, List<Tuple<DateTime, string>>> Alarms { get; private set; }
        public string Patient { get; set; }
        private Thread _interfaceThread;
        private dynamic _ui;
        private MonitorServiceClient _svc;
        private List<Sensor> _sensors;
        private List<string> _subscriptions;
        private Random _random;
        private int _nextAlarmID;
        private int NextAlarmID
        {
            get
            {
                return _nextAlarmID += 1;
            }
        }
        public Action<string, string, double> UpdateVital { get; set; }
        public Action<string, string> UpdateAlarm { get; set; }
        public Action<string, string> UpdateAcknowledgement { get; set; }
        #endregion


        public GenericMonitor(bool publisher, bool text)
        {
            this.IsPublisher = publisher;
            this.IsText = text;
            _random = new Random();
            _nextAlarmID = 0;
        }


        public void Start()
        {
            // connect to the central server
            _svc = new MonitorServiceClient(new InstanceContext(this));
            _svc.Open();

            // initialize the ui
            if (this.IsText)
                _ui = new MonitorTUI.UserInterface(this);
            else
                _ui = new MonitorGUI.UserInterface(this);

            _ui.IsBedsideMonitor = this.IsPublisher;

            // pass callback references to the ui
            _ui.Subscribe += (Action<string>)Subscribe;
            _ui.Unsubscribe += (Action<string>)Unsubscribe;
            _ui.Acknowledge += (Action<string>)Acknowledge;
            _ui.Alarm += (Action<string>)Alarm;
            _ui.SetLimits += (Action<string, double, double>)SetLimits;
            _ui.Call += (Action)Call;

            // start the ui thread
            _interfaceThread = new Thread(new ThreadStart(this.Go));
            _interfaceThread.Start();

            // start sensor threads
            if (this.IsPublisher)
            {
                _sensors = new List<Sensor>();
                _sensors.Add(SensorGenerator("RespiratoryRate"));
                _sensors.Add(SensorGenerator("HeartRate"));
                _sensors.Add(SensorGenerator("BloodPressure"));
                _sensors.Add(SensorGenerator("Weight"));

                Subscribe(this.Patient);
            }

            // wait for the ui to close
            _interfaceThread.Join();

            // unsubscribe from any subscribed channels
            if (_subscriptions != null && _subscriptions.Count > 0)
            {
                foreach (var channel in _subscriptions)
                    _svc.Unsubscribe(channel);
            }

            // stop all of the sensors
            if (this.IsPublisher)
            {
                foreach (var sensor in _sensors)
                    sensor.Stop();

                foreach (var sensor in _sensors)
                    sensor.Join();
            }

            // close connection to the service
            _svc.Close();
        }


        #region Helper Methods
        public void Go()
        {
            _ui.Go();
        }

        private void Subscribe(string channel)
        {
            if (_subscriptions == null)
                _subscriptions = new List<string>();
            _svc.Subscribe(channel);
            _subscriptions.Add(channel);
        }

        private void Unsubscribe(string channel)
        {
            if (_subscriptions == null)
                return;
            _svc.Unsubscribe(channel);
            _subscriptions.Remove(channel);
        }

        private void Alarm(string alarm)
        {
            if (!this.IsPublisher)
                return;
            _svc.PublishAlarm(this.Patient, alarm);
        }

        private void Acknowledge(string alarm)
        {
            if (!this.IsPublisher)
                return;
            _svc.PublishAcknowledgement(this.Patient, alarm);
        }

        private void SetLimits(string vital, double lower, double upper)
        {
            if (!this.IsPublisher)
                return;
            var sensor = _sensors.First(s => s.Vital == vital);
            sensor.Upper = upper;
            sensor.Lower = lower;
        }

        private void Call()
        {
            if (!this.IsPublisher)
                return;
            _svc.PublishAlarm(this.Patient, "Call" + this.NextAlarmID);
        }

        private Sensor SensorGenerator(string vital)
        {
            return new Sensor(vital, SendVitalClosure(vital), _random);
        }

        private Action<double> SendVitalClosure(string vital)
        {
            return value => {
                value = (value / 100) * VitalScale.Max(vital);
                _svc.PublishVital(this.Patient, vital, value);
                if (value < VitalScale.Lower(vital) || value > VitalScale.Upper(vital))
                {
                    _svc.PublishAlarm(this.Patient, vital + this.NextAlarmID);
                }
            };
        }
        #endregion

        #region Monitor Service Callbacks
        public void ReceiveVital(string bedsideMonitor, string vital, double value)
        {
            if (this.Vitals == null)
                this.Vitals = new Dictionary<string, Dictionary<string, List<Tuple<DateTime, double>>>>();

            var channel = this.Vitals.GetValueOrInsert(bedsideMonitor, new Dictionary<string, List<Tuple<DateTime, double>>>());
            var list = channel.GetValueOrInsert(vital, new List<Tuple<DateTime, double>>());
            list.Add(new Tuple<DateTime, double>(DateTime.Now, value));
            if (list.Count > MaxVitalsToKeep)
                list.RemoveAt(0);

            if (UpdateVital != null)
                UpdateVital(bedsideMonitor, vital, value);
        }

        public void ReceiveAlarm(string bedsideMonitor, string alarm)
        {
            if (this.Alarms == null)
                this.Alarms = new Dictionary<string, List<Tuple<DateTime, string>>>();

            var list = this.Alarms.GetValueOrInsert(bedsideMonitor, new List<Tuple<DateTime, string>>());
            list.Add(new Tuple<DateTime, string>(DateTime.Now, alarm));

            if (UpdateAlarm != null)
                UpdateAlarm(bedsideMonitor, alarm);
        }

        public void ReceiveAcknowledgement(string bedsideMonitor, string acknowledgement)
        {
            if (this.Alarms == null || !this.Alarms.ContainsKey(bedsideMonitor))
                return;

            this.Alarms[bedsideMonitor].RemoveAll(t => t.Item2 == acknowledgement);

            if (UpdateAcknowledgement != null)
                UpdateAcknowledgement(bedsideMonitor, acknowledgement);
        }
        #endregion
    }
}
