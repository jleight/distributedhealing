﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public static class ExtensionMethods
    {
        public static TValue GetValueOrInsert<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : (dictionary[key] = defaultValue);
        }
    }
}
