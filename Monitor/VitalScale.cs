﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public class VitalScale
    {
        public static double Max(string vital)
        {
            switch (vital)
            {
                case "BloodPressure":
                    return 200.0;
                case "HeartRate":
                    return 200.0;
                case "RespiratoryRate":
                    return 100.0;
                case "Weight":
                    return 2000.0;
                default:
                    return 100.0;
            }
        }

        public static double Upper(string vital)
        {
            switch (vital)
            {
                case "BloodPressure":
                    return 120.0;
                case "HeartRate":
                    return 105.0;
                case "RespiratoryRate":
                    return 20.0;
                case "Weight":
                    return 220.0;
                default:
                    return 90.0;
            }
        }

        public static double Lower(string vital)
        {
            switch (vital)
            {
                case "BloodPressure":
                    return 110.0;
                case "HeartRate":
                    return 55.0;
                case "RespiratoryRate":
                    return 12.0;
                case "Weight":
                    return 110.0;
                default:
                    return 10.0;
            }
        }

        public static double Min(string vital)
        {
            switch (vital)
            {
                case "BloodPressure":
                    return 0.0;
                case "HeartRate":
                    return 0.0;
                case "RespiratoryRate":
                    return 0.0;
                case "Weight":
                    return 1.0;
                default:
                    return 100.0;
            }
        }
    }
}
