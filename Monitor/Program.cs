﻿using System;
using System.Linq;

namespace Monitor
{
    class Program
    {
        public static void Main(string[] args)
        {
            // check command line args
            var publisher = args.Contains("-publisher");
            var text = args.Contains("-text");

            // make a monitor
            var monitor = new GenericMonitor(publisher, text);

            // set the patient's name
            if (publisher)
            {
                monitor.Patient = args[1 + Array.IndexOf(args, "-publisher")];
            }

            // start the monitor
            monitor.Start();
        }
    }
}
