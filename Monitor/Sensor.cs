﻿using System;
using System.Threading;

namespace Monitor
{
    public class Sensor
    {
        #region Variables
        public string Vital { get; private set; }
        public Action<double> SendVital { get; set; }
        public int PollTime { get; set; }
        private Thread _thread;
        private bool _done;
        private Random _random;

        private double _upper;
        private double _lower;
        private bool _upperSet;
        private bool _lowerSet;
        public double Upper
        {
            get
            {
                return _upperSet ? _upper : VitalScale.Upper(this.Vital);
            }
            set
            {
                _upperSet = true;
                _upper = value;
            }
        }
        public double Lower
        {
            get
            {
                return _lowerSet ? _lower : VitalScale.Lower(this.Vital);
            }
            set
            {
                _lowerSet = true;
                _lower = value;
            }
        }
        #endregion


        public Sensor(string vital, Action<double> sendVital, Random random)
        {
            this.Vital = vital;
            this.SendVital = sendVital;
            _done = false;
            _random = random;
            this.PollTime = 1000;

            _thread = new Thread(new ThreadStart(this.GenerateVitals));
            _thread.Start();
        }


        private void GenerateVitals()
        {
            while (!_done)
            {
                Thread.Sleep(this.PollTime);
                SendVital((GetMeasurement() / VitalScale.Max(this.Vital)) * 100);
            }
        }

        private double GetMeasurement()
        {
            var rand = _random.NextDouble() * 1000;
            if (rand < 1)
                return _random.NextDouble() * (this.Lower - VitalScale.Min(this.Vital)) + VitalScale.Min(this.Vital);
            if (rand > 999)
                return _random.NextDouble() * (VitalScale.Max(this.Vital) - this.Upper) + this.Upper;
            return (_random.NextDouble() * (this.Upper - this.Lower)) + this.Lower;
        }

        public void Stop()
        {
            _done = true;
        }

        public void Join()
        {
            _thread.Join();
        }
    }
}
