﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Monitor.MonitorService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MonitorService.IMonitorService", CallbackContract=typeof(Monitor.MonitorService.IMonitorServiceCallback), SessionMode=System.ServiceModel.SessionMode.Required)]
    public interface IMonitorService {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/Subscribe")]
        void Subscribe(string channel);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/Unsubscribe")]
        void Unsubscribe(string channel);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/PublishVital")]
        void PublishVital(string channel, string vital, double value);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/PublishAlarm")]
        void PublishAlarm(string channel, string alarm);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/PublishAcknowledgement")]
        void PublishAcknowledgement(string channel, string acknowledgement);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMonitorServiceCallback {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/ReceiveVital")]
        void ReceiveVital(string bedsideMonitor, string vital, double value);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/ReceiveAlarm")]
        void ReceiveAlarm(string bedsideMonitor, string alarm);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IMonitorService/ReceiveAcknowledgement")]
        void ReceiveAcknowledgement(string bedsideMonitor, string acknowledgement);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMonitorServiceChannel : Monitor.MonitorService.IMonitorService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MonitorServiceClient : System.ServiceModel.DuplexClientBase<Monitor.MonitorService.IMonitorService>, Monitor.MonitorService.IMonitorService {
        
        public MonitorServiceClient(System.ServiceModel.InstanceContext callbackInstance) : 
                base(callbackInstance) {
        }
        
        public MonitorServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
                base(callbackInstance, endpointConfigurationName) {
        }
        
        public MonitorServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public MonitorServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public MonitorServiceClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, binding, remoteAddress) {
        }
        
        public void Subscribe(string channel) {
            base.Channel.Subscribe(channel);
        }
        
        public void Unsubscribe(string channel) {
            base.Channel.Unsubscribe(channel);
        }
        
        public void PublishVital(string channel, string vital, double value) {
            base.Channel.PublishVital(channel, vital, value);
        }
        
        public void PublishAlarm(string channel, string alarm) {
            base.Channel.PublishAlarm(channel, alarm);
        }
        
        public void PublishAcknowledgement(string channel, string acknowledgement) {
            base.Channel.PublishAcknowledgement(channel, acknowledgement);
        }
    }
}
