﻿namespace MonitorGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subscribeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsubscribeCurrentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setVitalLimitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.bedsLabel = new System.Windows.Forms.Label();
            this.bedsList = new System.Windows.Forms.FlowLayoutPanel();
            this.vitalsContainer = new System.Windows.Forms.SplitContainer();
            this.patientCallButton = new System.Windows.Forms.Button();
            this.vitalsLabel = new System.Windows.Forms.Label();
            this.vitalsSelectionLayout = new System.Windows.Forms.TabControl();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vitalsContainer)).BeginInit();
            this.vitalsContainer.Panel1.SuspendLayout();
            this.vitalsContainer.Panel2.SuspendLayout();
            this.vitalsContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.simulationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(883, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.subscribeToolStripMenuItem,
            this.unsubscribeCurrentToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // subscribeToolStripMenuItem
            // 
            this.subscribeToolStripMenuItem.Name = "subscribeToolStripMenuItem";
            this.subscribeToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.subscribeToolStripMenuItem.Text = "Subscribe";
            this.subscribeToolStripMenuItem.Click += new System.EventHandler(this.subscribeToolStripMenuItem_Click);
            // 
            // unsubscribeCurrentToolStripMenuItem
            // 
            this.unsubscribeCurrentToolStripMenuItem.Name = "unsubscribeCurrentToolStripMenuItem";
            this.unsubscribeCurrentToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.unsubscribeCurrentToolStripMenuItem.Text = "Unsubscribe Current";
            this.unsubscribeCurrentToolStripMenuItem.Click += new System.EventHandler(this.unsubscribeCurrentToolStripMenuItem_Click);
            // 
            // simulationToolStripMenuItem
            // 
            this.simulationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setVitalLimitsToolStripMenuItem});
            this.simulationToolStripMenuItem.Name = "simulationToolStripMenuItem";
            this.simulationToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.simulationToolStripMenuItem.Text = "Simulation";
            // 
            // setVitalLimitsToolStripMenuItem
            // 
            this.setVitalLimitsToolStripMenuItem.Name = "setVitalLimitsToolStripMenuItem";
            this.setVitalLimitsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.setVitalLimitsToolStripMenuItem.Text = "Set Vital Limits";
            this.setVitalLimitsToolStripMenuItem.Click += new System.EventHandler(this.setVitalLimitsToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.vitalsContainer);
            this.splitContainer1.Size = new System.Drawing.Size(883, 434);
            this.splitContainer1.SplitterDistance = 322;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.bedsLabel);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.bedsList);
            this.splitContainer2.Size = new System.Drawing.Size(322, 434);
            this.splitContainer2.SplitterDistance = 30;
            this.splitContainer2.TabIndex = 0;
            // 
            // bedsLabel
            // 
            this.bedsLabel.AutoSize = true;
            this.bedsLabel.Location = new System.Drawing.Point(94, 10);
            this.bedsLabel.Name = "bedsLabel";
            this.bedsLabel.Size = new System.Drawing.Size(31, 13);
            this.bedsLabel.TabIndex = 0;
            this.bedsLabel.Text = "Beds";
            // 
            // bedsList
            // 
            this.bedsList.AutoScroll = true;
            this.bedsList.BackColor = System.Drawing.SystemColors.Control;
            this.bedsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bedsList.Location = new System.Drawing.Point(0, 0);
            this.bedsList.Name = "bedsList";
            this.bedsList.Size = new System.Drawing.Size(322, 400);
            this.bedsList.TabIndex = 0;
            // 
            // vitalsContainer
            // 
            this.vitalsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vitalsContainer.IsSplitterFixed = true;
            this.vitalsContainer.Location = new System.Drawing.Point(0, 0);
            this.vitalsContainer.Name = "vitalsContainer";
            this.vitalsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // vitalsContainer.Panel1
            // 
            this.vitalsContainer.Panel1.Controls.Add(this.patientCallButton);
            this.vitalsContainer.Panel1.Controls.Add(this.vitalsLabel);
            // 
            // vitalsContainer.Panel2
            // 
            this.vitalsContainer.Panel2.Controls.Add(this.vitalsSelectionLayout);
            this.vitalsContainer.Size = new System.Drawing.Size(557, 434);
            this.vitalsContainer.SplitterDistance = 30;
            this.vitalsContainer.TabIndex = 0;
            // 
            // patientCallButton
            // 
            this.patientCallButton.Location = new System.Drawing.Point(330, 2);
            this.patientCallButton.Name = "patientCallButton";
            this.patientCallButton.Size = new System.Drawing.Size(86, 23);
            this.patientCallButton.TabIndex = 2;
            this.patientCallButton.Text = "Patient Call";
            this.patientCallButton.UseVisualStyleBackColor = true;
            this.patientCallButton.Click += new System.EventHandler(this.patientCallButton_Click);
            // 
            // vitalsLabel
            // 
            this.vitalsLabel.AutoSize = true;
            this.vitalsLabel.Location = new System.Drawing.Point(199, 7);
            this.vitalsLabel.Name = "vitalsLabel";
            this.vitalsLabel.Size = new System.Drawing.Size(32, 13);
            this.vitalsLabel.TabIndex = 1;
            this.vitalsLabel.Text = "Vitals";
            // 
            // vitalsSelectionLayout
            // 
            this.vitalsSelectionLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vitalsSelectionLayout.Location = new System.Drawing.Point(0, 0);
            this.vitalsSelectionLayout.Name = "vitalsSelectionLayout";
            this.vitalsSelectionLayout.SelectedIndex = 0;
            this.vitalsSelectionLayout.Size = new System.Drawing.Size(557, 400);
            this.vitalsSelectionLayout.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 458);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "BedsideMonitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.vitalsContainer.Panel1.ResumeLayout(false);
            this.vitalsContainer.Panel1.PerformLayout();
            this.vitalsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vitalsContainer)).EndInit();
            this.vitalsContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer vitalsContainer;
        private System.Windows.Forms.Label bedsLabel;
        private System.Windows.Forms.Label vitalsLabel;
        private System.Windows.Forms.FlowLayoutPanel bedsList;
        private System.Windows.Forms.ToolStripMenuItem subscribeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsubscribeCurrentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simulationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setVitalLimitsToolStripMenuItem;
        private System.Windows.Forms.Button patientCallButton;
        private System.Windows.Forms.TabControl vitalsSelectionLayout;
    }
}

