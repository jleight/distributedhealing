﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using System.Threading;

namespace MonitorGUI
{
    public partial class MainForm : Form
    {
        private bool isDone;
        private UserInterface userInterface;
        private BedPanelControl currentlySelected = null;
        private BedPanelControl CurrentlySelected
        {
            get { return currentlySelected; }

            set
            {
                currentlySelected = value;
                //userInterface.Command_UpdateVitals(currentlySelected.BedUID, this.vitalsListBox);
            }
        }

        #region Callbacks

        public Action<string, string, double> UpdateVitalsFunc
        {
            get {
                return (Action<string,string,double>)updateVitals;
            }
        }

        public Action<string, string> UpdateAlarmFunc
        {
            get {
                return (Action<string, string>)updateAlarm;
            }
        }

        public Action<string, string> UpdateAcknowledgementFunc
        {
            get
            {
                return (Action<string, string>)updateAcknowledgement;
            }
        }

        #endregion

        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(UserInterface userInterface) : this()
        {
            // TODO: Complete member initialization
            this.userInterface = userInterface;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if(userInterface.IsBedsideMonitor) {
                this.subscribeToolStripMenuItem.Enabled = false;
                this.unsubscribeCurrentToolStripMenuItem.Enabled = false;
            }
            else if (userInterface.IsCentralMonitor)
            {
                this.setVitalLimitsToolStripMenuItem.Enabled = false;
                this.patientCallButton.Visible = false;
            }
        }

        public void addBed(string patient)
        {
            BedPanelControl newBed = new BedPanelControl();
            newBed.PatientName = patient;
            newBed.BedUID = patient;
            newBed.setSelectionCallback(new EventHandler(patientBed_Click));

            bedsList.Controls.Add(newBed);

            if (userInterface.IsBedsideMonitor)
            {
                newBed.AlarmAcknowledgement = new EventHandler(acknowledgeAlarm);
            }

            CurrentlySelected = newBed;
        }

        private void removeBed(BedPanelControl bed)
        {
            bedsList.Controls.Remove(bed);
        }

        private void patientBed_Click(object sender, EventArgs e)
        {
            if (CurrentlySelected != null)
            {
                CurrentlySelected.BackColor = System.Drawing.SystemColors.Control;
            }

            CurrentlySelected = (sender as Panel).Parent.Parent as BedPanelControl;
            CurrentlySelected.BackColor = Color.Aqua;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void subscribeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string patient = Microsoft.VisualBasic.Interaction.InputBox("Patient: ", "Subscribe", null);

            userInterface.Command_Subscribe(patient);
            addBed(patient);
        }

        private void unsubscribeCurrentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentlySelected != null)
            {
                userInterface.Command_Unsubscribe(currentlySelected.BedUID);
                removeBed(currentlySelected);
                currentlySelected = null;
            }
        }

        public void displayAlarm(string patient, string alarm)
        {
            foreach (var patientControl in bedsList.Controls)
            {
                var monitorControl = patientControl as BedPanelControl;
                if (monitorControl != null && monitorControl.BedUID == patient)
                {
                    MethodInvoker method = delegate
                    {
                        monitorControl.AlarmButton.Visible = true;
                    };
                    if (InvokeRequired)
                        BeginInvoke(method);
                    else
                        method.Invoke();
                }
            }
        }

        private void setVitalLimitsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (vitalsSelectionLayout.TabPages.Count > 0)
            {
                string vital = vitalsSelectionLayout.SelectedTab.Text;
                string lower = Microsoft.VisualBasic.Interaction.InputBox("Lower Limit: ", "Set Limits", null);
                string upper = Microsoft.VisualBasic.Interaction.InputBox("Upper Limit: ", "Set Limits", null);
                userInterface.Command_Limit(vital, lower, upper);
            }
        }

        private void patientCallButton_Click(object sender, EventArgs e)
        {
            userInterface.Command_Call();
        }

        private void acknowledgeAlarm(object sender, EventArgs e)
        {
            CurrentlySelected = (sender as Button).Parent.Parent.Parent as BedPanelControl;
            userInterface.Command_Ack(currentlySelected.BedUID);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.isDone = true;
        }

        private void updateVitals(string channel, string vitalName, double value)
        {
            if (currentlySelected != null && (currentlySelected.BedUID == channel))
            {
                MethodInvoker method = delegate
                {
                    if (!vitalsSelectionLayout.TabPages.ContainsKey(vitalName))
                    {
                        vitalsSelectionLayout.TabPages.Add(vitalName, vitalName);
                    }

                    var records = userInterface.RecordedVitals[channel];

                    var page = vitalsSelectionLayout.TabPages[vitalName];
                    if (vitalsSelectionLayout.SelectedTab == page)
                    {
                        Graphics g = page.CreateGraphics();
                        g.Clear(Color.Black);
                        

                        //draw the bounding lines
                        int graphSpan = (int)g.VisibleClipBounds.Width;
                        float useableHeight = g.VisibleClipBounds.Height - 50;
                        float baseLine = g.VisibleClipBounds.Top + (useableHeight * 2 / 3);

                        Pen pen = new Pen(new SolidBrush(Color.White), 1.0f);

                        //draw mid-line
                        g.DrawLine(pen, 0, baseLine, graphSpan, baseLine);

                        //draw some stats
                        g.DrawString("Current: " + value, new Font("Cambria", 12), new SolidBrush(Color.White), new PointF(5, baseLine + 5));

                        var record = records[vitalName];
                        //get the maximum for scaling
                        double maxValue = record.Max(x => x.Item2);
                        //get the average
                        double avgValue = record.Average(x => x.Item2);

                        g.DrawString("Recent Max: " + maxValue, new Font("Cambria", 12), new SolidBrush(Color.White), new PointF(5, baseLine + 25));
                        g.DrawString("Recent Avg: " + avgValue, new Font("Cambria", 12), new SolidBrush(Color.White), new PointF(5, baseLine + 45));

                        pen.Color = Color.Red;


                        Point tail = new Point(graphSpan - 20, (int)(baseLine - (record.Last().Item2 / maxValue) * (baseLine - 25)));
                        for(int i = record.Count - 2; i >= 0; i--)
                        {
                            double scaled = (record[i].Item2 / maxValue) * (baseLine - 25);
                            Point head = new Point(tail.X - 20, (int)(baseLine - scaled));
                            g.DrawLine(pen, tail, head);
                            tail = head;
                        }

                    }
                    
                };
                if (InvokeRequired)
                    BeginInvoke(method);
                else
                    method.Invoke();
            }
        }

        private void updateAlarm(string channel, string alarm)
        {
            //update alarms
            MethodInvoker method = delegate
            {
                foreach (var panelControl in this.bedsList.Controls)
                {
                    var bpc = panelControl as BedPanelControl;
                    bpc.AlarmButton.Visible = (bpc.BedUID == channel);
                    break;
                }
            };
            if (InvokeRequired)
                BeginInvoke(method);
            else
                method.Invoke();
        }

        private void updateAcknowledgement(string channel, string acknowledgement) {

        }
    }
}
