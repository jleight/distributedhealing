﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MonitorGUI
{
    public partial class BedPanelControl : UserControl
    {
        public EventHandler AlarmAcknowledgement { get; set; }

        public string PatientName
        {
            get
            {
                return this.bedNameLabel.Text;
            }

            set
            {
                this.bedNameLabel.Text = value;
            }
        }

        public string BedUID
        {
            get
            {
                return this.bedDetailsGroup.Text;
            }

            set
            {
                this.bedDetailsGroup.Text = value;
            }
        }

        public Button AlarmButton
        {
            get
            {
                return this.alarmButton;
            }
        }

        public BedPanelControl()
        {
            InitializeComponent();
        }

        public void setSelectionCallback(EventHandler handler)
        {
            //selectionPanel.Click += handler;
            this.tableLayoutPanel1.Click += handler;
        }

        private void alarmButton_Click(object sender, EventArgs e)
        {
            if (AlarmAcknowledgement != null)
            {
                AlarmAcknowledgement(sender, e);
                this.alarmButton.Visible = false;
            }
        }

        private void BedPanelControl_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.Aqua;
        }

    }
}
