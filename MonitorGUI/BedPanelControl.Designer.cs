﻿namespace MonitorGUI
{
    partial class BedPanelControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bedDetailsGroup = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.bedNameLabel = new System.Windows.Forms.Label();
            this.alarmButton = new System.Windows.Forms.Button();
            this.bedDetailsGroup.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bedDetailsGroup
            // 
            this.bedDetailsGroup.Controls.Add(this.tableLayoutPanel1);
            this.bedDetailsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bedDetailsGroup.Location = new System.Drawing.Point(0, 0);
            this.bedDetailsGroup.Name = "bedDetailsGroup";
            this.bedDetailsGroup.Size = new System.Drawing.Size(294, 64);
            this.bedDetailsGroup.TabIndex = 0;
            this.bedDetailsGroup.TabStop = false;
            this.bedDetailsGroup.Text = "BedUID Goes Here";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.bedNameLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.alarmButton, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(288, 45);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // bedNameLabel
            // 
            this.bedNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bedNameLabel.AutoSize = true;
            this.bedNameLabel.Location = new System.Drawing.Point(20, 16);
            this.bedNameLabel.Name = "bedNameLabel";
            this.bedNameLabel.Size = new System.Drawing.Size(104, 13);
            this.bedNameLabel.TabIndex = 6;
            this.bedNameLabel.Text = "Patient\'s Name Here";
            // 
            // alarmButton
            // 
            this.alarmButton.BackColor = System.Drawing.Color.Red;
            this.alarmButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alarmButton.Location = new System.Drawing.Point(147, 3);
            this.alarmButton.Name = "alarmButton";
            this.alarmButton.Size = new System.Drawing.Size(138, 39);
            this.alarmButton.TabIndex = 7;
            this.alarmButton.Text = "Alarm!";
            this.alarmButton.UseVisualStyleBackColor = false;
            this.alarmButton.Visible = false;
            this.alarmButton.Click += new System.EventHandler(this.alarmButton_Click);
            // 
            // BedPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.bedDetailsGroup);
            this.Name = "BedPanelControl";
            this.Size = new System.Drawing.Size(294, 64);
            this.MouseEnter += new System.EventHandler(this.BedPanelControl_MouseEnter);
            this.bedDetailsGroup.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox bedDetailsGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label bedNameLabel;
        private System.Windows.Forms.Button alarmButton;
    }
}
