﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;


namespace MonitorGUI
{
    public class UserInterface
    {
        #region Variables
        public bool IsBedsideMonitor { get; set; }
        public bool IsCentralMonitor
        {
            get
            {
                return !IsBedsideMonitor;
            }
            set
            {
                IsBedsideMonitor = !value;
            }
        }

        private dynamic _monitor;

        public Dictionary<string, Dictionary<string, List<Tuple<DateTime, double>>>> RecordedVitals
        {
            get
            {
                return _monitor.Vitals;
            }
        }

        public Action<string> Subscribe { get; set; }
        public Action<string> Unsubscribe { get; set; }
        public Action<string> Acknowledge { get; set; }
        public Action<string> Alarm { get; set; }
        public Action<string, double, double> SetLimits { get; set; }
        public Action Call { get; set; }
        public MainForm MyForm { get; private set; }
        #endregion

        public UserInterface(dynamic monitor)
        {
            _monitor = monitor;
        }

        public void Command_Subscribe(string channel)
        {
            if (channel == null)
            {
                return;
            }

            if (Subscribe == null)
            {
                return;
            }

            Subscribe(channel);
        }

        public void Command_Unsubscribe(string channel)
        {
            if (channel == null)
            {
                return;
            }

            if (Unsubscribe == null)
            {
                return;
            }

            Unsubscribe(channel);
        }

        public void Command_Ack(string channel)
        {
            if (this.IsCentralMonitor)
            {
                return;
            }

            foreach (var alarm in _monitor.Alarms[channel])
                Acknowledge(alarm.Item2);
        }

        public void Command_TriggerAlarm(string alarm)
        {
            if (this.IsCentralMonitor)
            {
                return;
            }

            Alarm(alarm);
        }

        public void Command_Limit(string vital, string strLower, string strUpper)
        {
            if (this.IsCentralMonitor)
            {
                return;
            }

            double lower;
            double upper;
            if (!Double.TryParse(strLower, out lower) || !Double.TryParse(strUpper, out upper))
            {
                return;
            }

            SetLimits(vital, lower, upper);
        }

        public void Command_Call()
        {
            if (this.IsCentralMonitor)
                return;

            Call();
        }

        public void Go()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MyForm = new MainForm(this);
            if (this.IsBedsideMonitor)
                MyForm.addBed(_monitor.Patient);

            _monitor.UpdateVital += MyForm.UpdateVitalsFunc;
            _monitor.UpdateAlarm += MyForm.UpdateAlarmFunc;
            _monitor.UpdateAcknowledgement += MyForm.UpdateAcknowledgementFunc;
            Application.Run(MyForm);
        }

        public void Command_UpdateVitals(string channel, Dictionary<string, Tuple<DateTime, double>> vitalsDictionary)
        {
            vitalsDictionary.Clear();

            if (_monitor.Vitals == null || !_monitor.Vitals.ContainsKey(channel))
            {
                return;
            }

            var vitals = _monitor.Vitals[channel];
            foreach (var vital in vitals)
            {
                var list = vital.Value.ToArray();
                var value = list[list.Length - 1];
                vitalsDictionary[vital.Key] = value;
            }
        }

        public void DisplayAlarm(string channel, string alarm)
        {
           //NOTHINGNNNN
        }

        public List<string> gatherAlarms()
        {
            if (_monitor.Alarms == null)
                return null;

            var strings = new List<string>();
            foreach (var channel in _monitor.Alarms)
            {
                if (channel.Value.Count > 0)
                    strings.Add(channel.Key);
            }
            return strings;
        }
    }

}
